/*
 * Spawner with some options. Can be controlled by ActionSendMessage.
 * 
 * Part of EAFramework
 * 
 * Created by: Pierre Rossel 2021-04-20
 * 
 */

using UnityEngine;

public class Spawner : MonoBehaviour
{
    public bool spawning = false;

    public Transform spawnPosition;
    public Vector3 spawnPositionOffset;

    [Tooltip("Spawn rate (items per second)")]
    public float rate = 2;

    [Tooltip("Do not spawn when too close from previous spawned object.")]
    public float distanceMinFromPrevious = 0.1f;

    // Start is called before the first frame update
    public GameObject[] objectsToSpawn;

    // Not public variables

    float nextSpawnTime = 0;
    Vector3 lastSpawnPosition;

    void Start()
    {
        if (spawnPosition == null)
        {
            spawnPosition = transform;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (spawning && Time.time > nextSpawnTime)
        {
            SpawnOne();
        }
    }

    void SpawnOne()
    {
        //Debug.Log("SpawnOne()");

        Vector3 realSpawnPosition = spawnPosition.position + spawnPositionOffset;

        // If real spawn position is far enough from last position, spawn an new one
        if (Vector3.Distance(realSpawnPosition, lastSpawnPosition) > distanceMinFromPrevious) {

            Debug.Log("Spawning");

            foreach (GameObject obj in objectsToSpawn)
            {
                Instantiate(obj, spawnPosition.position + spawnPositionOffset, transform.rotation);
            }

            lastSpawnPosition = realSpawnPosition;

            nextSpawnTime = Time.time + 1 / rate;
        }
    }

    void SpawnStart()
    {
        Debug.Log("SpawnStart()");

        //nextSpawnTime = Mathf.Max(nextSpawnTime, Time.time);

        spawning = true;
    }

    void SpawnStop()
    {
        Debug.Log("SpawnStop()");
        spawning = false;
    }

    void SpawnToggle()
    {
        spawning = !spawning;
    }

    void SetRate(float newRate)
    {
        rate = newRate;
    }
}
