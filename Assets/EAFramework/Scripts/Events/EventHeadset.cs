﻿/*
 * Allows to send custom events. 
 * 
 * Can be triggered from animation timeline.
 * 
 * Part of EAFramework
 * 
 * Created by: Pierre Rossel 2020-04-01
 * 
 */

using UnityEngine;
using Oculus;

namespace EAFramework
{

    public class EventHeadset : Event
    {
        [Header("Events")]
        public bool headsetMounted = true;
        public bool headsetUnmounted;

        [Space(10)]

        [Tooltip("Delay after which the event will be sent.")]
        public float sendDelay = 0;

        private void OnEnable()
        {
            Debug.Log("OVRManager.isHmdPresent: " + OVRManager.isHmdPresent);
            OVRManager.HMDMounted += OnHeadsetMounted;
            OVRManager.HMDUnmounted += OnHeadsetUnmounted;
        }

        private void OnDisable()
        {
            OVRManager.HMDMounted -= OnHeadsetMounted;
            OVRManager.HMDUnmounted -= OnHeadsetUnmounted;
        }

        void SendHeadsetMounted()
        {
            SendEvent("HeadsetMounted");
        }

        void SendHeadsetUnmounted()
        {
            SendEvent("HeadsetUnmounted");
        }

        // Oculus callbacks
        void OnHeadsetMounted()
        {
            Debug.Log("OVRManager.isHmdPresent: " + OVRManager.isHmdPresent);
            if (headsetMounted)
            {
                Invoke("SendHeadsetMounted", sendDelay);
            }
        }

        void OnHeadsetUnmounted()
        {
            Debug.Log("OVRManager.isHmdPresent: " + OVRManager.isHmdPresent);
            if (headsetUnmounted)
            {
                Invoke("SendHeadsetUnmounted", sendDelay);
            }
        }

    }

}
