﻿/*
 * Detects a keyboard event
 * 
 * Part of EAFramework
 * 
 * Created by: Pierre Rossel 2021-11-05
 * 
 */

using UnityEngine;

namespace EAFramework
{

    public class EventKeyboard : Event
    {
        public KeyCode key;
        public bool whenPressed = true;
        public bool whenReleased = false;
        public bool withControl = true;
        public bool withAlt = false;
        public bool withShift = false;


        [Tooltip("Delay after which the event will be sent.")]
        public float sendDelay = 0;

        private void Update()
        {
            if (whenPressed && Input.GetKeyDown(key) && ModifiersAreValid())
            {
                TriggerEventKeyboard();
            }

            if (whenReleased && Input.GetKeyUp(key))
            {
                TriggerEventKeyboard();
            }
        }

        bool ModifiersAreValid()
        {
            return (!withControl || Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl))
                && (!withAlt || Input.GetKey(KeyCode.LeftAlt) || Input.GetKey(KeyCode.RightAlt))
                && (!withShift || Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
                ;
        }


        void TriggerEventKeyboard()
        {
            Invoke("SendKeyboard", sendDelay);
        }

        void SendKeyboard()
        {
            SendEvent("Keyboard");
        }

    }

}
