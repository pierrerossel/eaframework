﻿/*
 * 
 * 
 * Part of EAFramework
 * 
 * Created by: Pierre Rossel 2020-04-01
 * 
 */

using UnityEngine;
using UnityEngine.SceneManagement;

namespace EAFramework
{
    public class ActionLoadScene : Action
    {

        [Tooltip("Scene name (must be in Build Settings) or leave empty to reload current scene.")]
        public string sceneName = "Scene name";

        override public void Execute(string eventName)
        {
            if (sceneName == "")
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }
            else
            {
                SceneManager.LoadScene(sceneName);
            }
        }
    }

}