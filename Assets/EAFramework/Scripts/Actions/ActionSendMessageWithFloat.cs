﻿/*
 * Action to send a message (= call a method) with a float parameter
 * 
 * Part of EAFramework
 * 
 * Created by: Pierre Rossel 2021-04-21
 * 
 */

using UnityEngine;

namespace EAFramework
{
    public class ActionSendMessageWithFloat : Action
    {
        public GameObject target;
        public string message;
        public float parameter;

        override public void Execute(string eventName)
        {
            // Get defaut action target from event source
            if (!target)
            {
                target = eventSource;
            }

            target.SendMessage(message, parameter);
        }
    }

}