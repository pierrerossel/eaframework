﻿/*
 * 
 * 
 * Part of EAFramework
 * 
 * Created by: Pierre Rossel 2020-04-01
 * 
 */

using UnityEngine;

namespace EAFramework
{
    public class ActionMoveToRotation : Action
    {
        public GameObject what;
        public Transform targetRotation;
        public float duration = 2;
        public EasingFunction.Ease easing = EasingFunction.Ease.Linear;
        public bool setKinematicWhileMoving = false;

        [Tooltip("Event name to send to the eventSource object at the end of the move.\nCan be catched with an EventCustom component to execute more actions after the end of the move.")]
        public string endEventCustom = "MoveToRotationDone";

        protected Quaternion startRotation;
        protected Quaternion endRotation;
        protected Rigidbody rbToMove;
        protected float startTime;
        protected EasingFunction.Function easingFunction; // cached at every execute
        protected bool wasKinematic;

        override public void Execute(string eventName)
        {
            if (what == null)
            {
                what = eventSource;
            }

            if (targetRotation == null)
            {
                targetRotation = transform;
            }

            rbToMove = what.GetComponent<Rigidbody>();
            startTime = Time.time;
            startRotation = what.transform.rotation;
            easingFunction = EasingFunction.GetEasingFunction(easing);
            endRotation = targetRotation.rotation;

            if (setKinematicWhileMoving)
            {
                wasKinematic = rbToMove.isKinematic;
                rbToMove.isKinematic = true;
            }

            // Apply immediately
            if (duration < 0.00001f)
            {
                ApplyProgress(1);
            }
        }

        private void Update()
        {
            if (startTime > 0)
            {
                ApplyProgress((Time.time - startTime) / duration);
            }
        }

        protected void ApplyProgress(float progress)
        {
            if (progress >= 1)
            {
                progress = 1;
                startTime = 0;
            }

            // Apply easing to progress
            float easedProgress = easingFunction(0, 1, progress);

            //Quaternion newRot = Quaternion.Lerp(startRotation, targetRotation.rotation, easedProgress);
            Quaternion newRot = Quaternion.Lerp(startRotation, endRotation, easedProgress);

            if (rbToMove != null)
            {
                rbToMove.MoveRotation(newRot);
            }
            else
            {
                what.transform.rotation = newRot;
            }

            if (startTime < float.Epsilon)
            {
                // Restore previous kinematic setting if we modified it
                if (setKinematicWhileMoving)
                {
                    rbToMove.isKinematic = wasKinematic;
                }

                // Notify end of move with a custom message
                if (!endEventCustom.Equals(""))
                {
                    EventCustom.TriggerEventCustom(endEventCustom, eventSource);
                }
            }
        }

    }
}