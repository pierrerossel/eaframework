﻿/*
 * Action to send a message (= call a method) with a string parameter
 * 
 * Part of EAFramework
 * 
 * Created by: Pierre Rossel 2021-04-21
 * 
 */

using UnityEngine;

namespace EAFramework
{
    public class ActionSendMessageWithString : Action
    {
        public GameObject target;
        public string message;
        public string parameter;

        override public void Execute(string eventName)
        {
            // Get defaut action target from event source
            if (!target)
            {
                target = eventSource;
            }

            target.SendMessage(message, parameter);
        }
    }

}