﻿/*
 * Action to send a message (= call a method) with a bool parameter
 * 
 * Part of EAFramework
 * 
 * Created by: Pierre Rossel 2021-04-21
 * 
 */

using UnityEngine;

namespace EAFramework
{
    public class ActionSendMessageWithBool : Action
    {
        public GameObject target;
        public string message;
        public bool parameter;

        override public void Execute(string eventName)
        {
            // Get defaut action target from event source
            if (!target)
            {
                target = eventSource;
            }

            target.SendMessage(message, parameter);
        }
    }

}