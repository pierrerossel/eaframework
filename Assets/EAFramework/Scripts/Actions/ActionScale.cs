﻿/*
 * 
 * 
 * Part of EAFramework
 * 
 * Created by: Pierre Rossel 2020-04-01
 * 
 */

using UnityEngine;
using System.Collections.Generic;

namespace EAFramework
{
    public class ActionScale : Action
    {
        public GameObject target;
        public float scaleFactor = 1.2f;
        public float duration;
        public EasingFunction.Ease easing = EasingFunction.Ease.Linear;

        protected Vector3 originalScale;
        protected Vector3 startScale;
        protected float currentScaleFactor;
        protected float startTime;
        protected EasingFunction.Function ease; // cached at every execute

        private static List<ActionScale> instances = new List<ActionScale>();

        private void OnEnable()
        {
            instances.Add(this);
        }

        private void OnDisable()
        {
            instances.Remove(this);
        }

        private void Start()
        {
            // Get defaut action target from event source  
            if (!target)
            {
                target = eventSource;
            }

            originalScale = target.transform.localScale;
        }

        override public void Execute(string eventName)
        {
            // Stop any current scaling on same target
            foreach (var scaler in instances)
            {
                if (scaler.target == this.target)
                {
                    scaler.Stop();
                }
            }
            startTime = Time.time;
            startScale = target.transform.localScale;
            ease = EasingFunction.GetEasingFunction(easing);

            // Apply immediate scale
            if (duration < 0.00001f)
            {
                target.transform.localScale = originalScale * scaleFactor;
            }
        }

        private void Update()
        {
            if (startTime > 0)
            {
                float progress = (Time.time - startTime) / duration;
                if (progress >= 1)
                {
                    progress = 1;
                    startTime = 0;
                }

                // Apply easing to progress
                progress = ease(0, 1, progress);

                //transform.localScale = originalScale * (1 + (scaleFactor - 1) * (Time.time - startTime) / duration);
                target.transform.localScale = startScale + (originalScale * scaleFactor - startScale) * progress;
            }
        }

        protected void Stop()
        {
            startTime = 0;
        }
    }

}