﻿/*
 * 
 * 
 * Part of EAFramework
 * 
 * Created by: Pierre Rossel 2020-04-01
 * 
 */

using UnityEngine;

namespace EAFramework
{
    public class ActionSpawn : Action
    {
        public Transform spawnPosition;
        public GameObject[] objectsToSpawn;

        private void Start()
        {
            if (spawnPosition == null)
            {
                spawnPosition = transform;
            }
        }


        override public void Execute(string eventName)
        {

            foreach (GameObject obj in objectsToSpawn)
            {
                Instantiate(obj, spawnPosition.position, transform.rotation);
            }
        }
    }

}