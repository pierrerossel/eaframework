# EAFramework demos files

Version: 1.8.0

This folder contains demonstration scenes for EAFramework.

## Install / update

Get the latest version from:
https://bitbucket.org/pierrerossel/eaframework/

## About EAFramework

Event -> Action classes to quickly get started creating interactions with Unity

EAFramework is a kit to easily create interactions using Unity without coding at first. However its open architecture allows to add more functionalities by adding scripts to connect to existing parts.

## Dependencies

### Oculus integration

Examples are using Oculus, and currently there is some code left that depend on it.

Install it from Asset Store

### ProBuilder

Examples depend on ProBuilder. Optional if you don't need it in your project
