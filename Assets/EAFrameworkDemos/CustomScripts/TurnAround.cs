using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnAround : MonoBehaviour
{
    public Vector3 speed = new Vector3(0, 1, 0);

    //public float speedX = 1;
    //public float speedY = 1;
    //public float speedZ = 1;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //transform.Rotate(speed.x, speed.y, speed.z);
        transform.Rotate(speed);
    }
}
