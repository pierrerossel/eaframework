using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpinDoctor : MonoBehaviour
{

    public bool spinning = false;
    public float speed = 10;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (spinning) {
            transform.Rotate(speed * Time.deltaTime, 0, 0);
        }
    }

    void StartSpinning()
    {
        spinning = true;
    }

    void SetSpeed(float newSpeed)
    {
        speed = newSpeed;
    }

}
