﻿# Headset removed dialog

This package provides a dialog that appears when the headset is removed and mounted. It allows to continue or to restart. It is usefull in exhibitions where people change often and may quit at anytime. This offers a new user a way to start from the begining.

## Installation

- Install / update EAFramework to v1.7.0
- Install Headset removed dialog (this package)
- Drag the Assets/Prefabs/Headset removed.prefab to Scene (near to camera, easier to test)

## Configuration

- In object `Headset removed/EA Place dialog`
  - Component `Action Move To Position`
    - Field `Target Position`
      - Select CenterEyeAnchor or other camera
  - Component `Action Move To Rotation`
    - Field `Target Rotation`
      - Select CenterEyeAnchor or other camera
- In object `Headset removed/EA Restart`
  - Component `Action Load Scene`
    - Field `Scene Name`
      - Leave empty to reload same scene or give scene name
        (must be in Build Settings > Scenes in build)

## Simulation

To simulate the headset in the editor and test the dialog, use the following keyboard shortcuts.

- Press Ctrl + H to simulate the heaset being unmounted
- Release Ctrl + H to simulate the heaset being mounted

The shortcut keys can be configured in `EA Keyboard Simulation`.