# EAFramework development project

This is the README of the development project. It is not distributed.

Version: See other README files below

See dedicated README files in both distributed packages:

- [Assets/EAFramework/README.md](Assets/EAFramework/README.md)
- [Assets/EAFrameworkDemos/README.md](Assets/EAFrameworkDemos/README.md)

## Install EAFramework in your project

1. Get the latest version of EAFramework_X.X.X.unitypackage from https://bitbucket.org/pierrerossel/eaframework/downloads/
1. Install it in your project
1. If you need the optional demo files, get the latest version of EAFrameworkDemos_X.X.X.unitypackage from the same place and install them the same way

## Produce a release

1. Commit all modified files first
1. Modify the version number in both README files
1. Export Package from Assets/EAFramework folder, without dependencies, save it in Releases/EAFramework_X.X.X.unitypackage with correct version number
1. Export Package from Assets/EAFrameworkDemos folder, without dependencies, save it in Releases/EAFrameworkDemos_X.X.X.unitypackage with correct version number
1. commit with message Version X.X.X
1. tag with label vX.X.X
1. push commits
1. open https://bitbucket.org/pierrerossel/eaframework/downloads/ and add the two new .unitypackage files
